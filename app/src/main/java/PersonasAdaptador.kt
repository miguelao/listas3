import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.edtm.listas.R

class PersonasAdaptador(private val activity: Activity, personas: List<Personas>): BaseAdapter() {

   // private val context : Context

    private var personas : ArrayList<Personas>

    init {
        //this.context = context
        this.personas = personas as ArrayList<Personas>
    }

    override fun getCount(): Int {

        return personas.size
    }

    override fun getItemId(position: Int): Long {

        return position.toLong()
    }

    override fun getItem(position: Int): Any {

        return position
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var vista : View
        vista = inflater.inflate(R.layout.elementos, null)
        val nombre = vista.findViewById<TextView>(R.id.nombre)
        if(!personas.isEmpty()){
            nombre.text = personas[position].nombre
            vista.setOnClickListener {
                val builder = AlertDialog.Builder(vista.context)
                builder.setTitle("Eliminar elemento")
                builder.setMessage("Desea eliminar el elemento?")

                builder.setPositiveButton("OK", DialogInterface.OnClickListener {
                        dialog: DialogInterface, which: Int ->
                    personas.removeAt(position)
                    notifyDataSetChanged()
                    Toast.makeText(vista.context, "Elemento eliminado", Toast.LENGTH_SHORT).show()

                })

                builder.setNegativeButton("Cancelar", DialogInterface.OnClickListener{
                        dialog: DialogInterface, which: Int ->
                    dialog.dismiss()
                })

                builder.show()
            }
        }

        return vista

    }

}