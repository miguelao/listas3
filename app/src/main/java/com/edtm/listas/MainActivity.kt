package com.edtm.listas

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import PersonasAdaptador
import Personas
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.widget.*

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //var listView: ListView? = null
    var personas = ArrayList<Personas>()
    var adapter: PersonasAdaptador? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            add()
        }

        adapter = PersonasAdaptador(this, personas)

        val listView = findViewById<ListView>(R.id.lista)

        listView.adapter = adapter
    }

    private fun add(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Agregar elementos")
//        builder.setMessage("")
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogLayout = inflater.inflate(R.layout.edit_alert, null)
        val nombre = dialogLayout.findViewById<EditText>(R.id.nombre)
//        val apellido = dialogLayout.findViewById<EditText>(R.id.apellidos)
//        val edad = dialogLayout.findViewById<EditText>(R.id.edad)
        builder.setView(dialogLayout)

        builder.setPositiveButton("OK", DialogInterface.OnClickListener {
                dialog:DialogInterface, which: Int ->
            var persona = Personas(nombre.text.toString(), "", "")
            personas.add(persona)
            Toast.makeText(this,"Agregado correctamente", Toast.LENGTH_SHORT).show()
//            adapter?.notifyDataSetChanged()
        })

        builder.setNegativeButton("Cancelar", DialogInterface.OnClickListener{
            dialog:DialogInterface, which: Int ->
            dialog.dismiss()
        })

        builder.show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
