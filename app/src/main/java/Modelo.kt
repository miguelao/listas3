class Personas{
    var nombre : String = ""
    var apellidos : String = ""
    var edad : String = ""

    constructor(){}

    constructor(nombre:String, apellidos:String, edad:String){
        this.nombre = nombre
        this.apellidos = apellidos
        this.edad = edad
    }

    override fun toString(): String {
        return "Personas(nombre='$nombre', apellidos='$apellidos', edad='$edad')"
    }
}